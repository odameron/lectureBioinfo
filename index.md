# Conseils de lecture en lien avec la bioinfo

## Livres

- **The StatQuest Illustrated Guide to Machine Learning** (Josh Starmer) [https://statquest.org/statquest-store/](https://statquest.org/statquest-store/): support/compléments aux (excellentes) vidéos de StatQuest (cf. section "Ressources Web")
- **Le siècle du gène** / **The century of the gene** (Evelyn Fox Keller) [https://www.hup.harvard.edu/catalog.php?isbn=9780674008250](https://www.hup.harvard.edu/catalog.php?isbn=9780674008250): On n'a toujours pas de définition précise de la notion de gène ; ce livre retrace l'historique des différents courants et points de vue (recommandé par Jacques Van Helden)
- **The language of life** (Francis Collins): la découverte du gène BRCA1 dans la susceptibilité au cancer du sein, et l'impact des tests qui en découlent. Francis Collins a ensuite dirigé le Human Genome Project sur le premier séquençage du génome humain.
- **Le jeu de la science et du hasard -- la statistique et le vivant** (Daniel Schwartz) une introduction très claire aux principales notions de statistique.
- **La barque de Delphes -- ce que révèle le texte des génomes** (Antoine Danchin) Je ne suis pas allé jusqu'au bout mais j'en garde plutôt un bon souvenir et du coup j'ai envie de m'y remettre.
- **Il était une fois le gène** (Siddhartha Mukherjee) L'histoire de la génétique ; prix Pulitzer.
- **Bioinformatics algorithms -- an active learning approach** (Phillip Compeau and Pavel Pevzner) complément au cours cousera ; vidéos très chouettes.
- **Linked** (Albert-Laszlo Barabasi) sur les réseaux scale-free.
    - contre-opinion : [The network nonsense of Albert-László Barabási (by Lior Pachter)](https://liorpachter.wordpress.com/2014/02/10/the-network-nonsense-of-albert-laszlo-barabasi/)
- **The Elements of Statistical Learning: Data Mining, Inference, and Prediction.**: [https://web.stanford.edu/~hastie/ElemStatLearn/](https://web.stanford.edu/~hastie/ElemStatLearn/) (Trevor Hastie, Robert Tibshirani and Jerome Friedman) très complet, disponible en PDF
- **Graph theory**: [http://diestel-graph-theory.com/](http://diestel-graph-theory.com/) (Reinhard Diestel) aussi disponible en PDF. **Difficile d'accès**
- **Théorie des graphes** (J.A Bondy et USR Murty) une traduction existe en Français : [http://www-sop.inria.fr/members/Frederic.Havet/Traduction-Bondy-Murty.pdf](http://www-sop.inria.fr/members/Frederic.Havet/Traduction-Bondy-Murty.pdf)
- **Machine learning avec scikit-learn** (Aurélien Géron)
- **Understanding bioinformatics** (Marketa Zvelebil and Jeremy O. Baum) explications très détaillées + mindmaps au début de chaque chapitre.
- **Speech and language processing** [https://web.stanford.edu/~jurafsky/slp3/ed3book.pdf](https://web.stanford.edu/~jurafsky/slp3/ed3book.pdf) (D. Jurafsky et J.H. Martin)
- **Probability Theory: The Logic of Science** (E. T. Jaynes): Un livre révolutionnaire pour beaucoup qui prend les statistiques à contrecourant qui pose les fondements d'une logique inférentielle". [Disp en PDF gratuit ](https://bayes.wustl.edu/etj/prob/book.pdf)
- **Genetics: A Conceptual Approach - 6ème edition** (de Benjamin Pierce): Un manuel de génétique très bien illustré et à jour avec des annecdotes très sympa en guise d'intro de chapitre [Amazon](https://www.amazon.fr/Genetics-Conceptual-Approach-Benjamin-Pierce/dp/1319153917/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=1W8J9L90OR287&keywords=genetics+a+conceptual+approach&qid=1563867933&s=gateway&sprefix=genetics+a+con%2Cenglish-books%2C265&sr=8-1)
- **Programmation en Python pour les sciences de la vie** (Patrick Fuchs, Pierre Poulain) [https://www.dunod.com/sciences-techniques/programmation-en-python-pour-sciences-vie](https://www.dunod.com/sciences-techniques/programmation-en-python-pour-sciences-vie)
- **An Introduction to Systems Biology: Design Principles of Biological Circuits** (Uri Alon) [https://www.weizmann.ac.il/mcb/UriAlon/introduction-systems-biology-design-principles-biological-circuits](https://www.weizmann.ac.il/mcb/UriAlon/introduction-systems-biology-design-principles-biological-circuits) (recommandé par Pierre Lemée)
- **Assieds-toi et écris ta thèse !"** (Geneviève Belleville) [https://www.pulaval.com/produit/assieds-toi-et-ecris-ta-these-trucs-pratiques-et-motivationnels](https://www.pulaval.com/produit/assieds-toi-et-ecris-ta-these-trucs-pratiques-et-motivationnels) et video [https://www.youtube.com/watch?v=qbQ02vJkXQw](https://www.youtube.com/watch?v=qbQ02vJkXQw) : pas de la bioinfo à proprement parler, mais pleins de bons conseils pour dépasser l'angoisse de la page blanche, par exemple pour le rapport de stage !
- **Vers une recherche reproductible : faire évoluer ses pratiques** (Loïc Desquilbet, Sabrina Granger, Boris Hejblum, Arnaud Legrand, Pascal Pernot, Nicolas Rougier). Disponible en PDF et EPUB à [https://rr-france.github.io/bookrr/](https://rr-france.github.io/bookrr/)
- **The cartoon guide to genetics** (Larry Gonick) [http://www.larrygonick.com/titles/science/the-cartoon-guide-to-genetics/]: une mise en BD reposant sur des informations rigoureuses, conseillé chaudement par Olivier Ridoux
- **Material and methods** (Pellichi) manga sympa sur un stagiaire et une doctorante dans un labo de biologie [https://pellichi.fr/](https://pellichi.fr/)
- **Protéines** (Sophie Sacquin-Mora, Antoine Taly et Anmryn)
    - Tome 1 **Protéines - un voyage au centre de la cellule** (Sophie Sacquin-Mora, Antoine Taly et Anmryn) [https://laboutique.edpsciences.fr/produit/1222/9782759826773/proteines-voyage-au-centre-de-la-cellule](https://laboutique.edpsciences.fr/produit/1222/9782759826773/proteines-voyage-au-centre-de-la-cellule)
    - Tome  **Protéines - le carnaval du vivant** (Sophie Sacquin-Mora et Anmryn) [https://laboutique.edpsciences.fr/product/1286/9782759827480/proteines-2](https://laboutique.edpsciences.fr/product/1286/9782759827480/proteines-2)
- **The New Turing Omnibus** (A. Dewdney) [https://us.macmillan.com/books/9780805071665/thenewturingomnibus](https://us.macmillan.com/books/9780805071665/thenewturingomnibus) plutôt sur le côté info, conseillé par Olivier Ridoux



## Ressources web
- **La Bioinformatique et la SFBI**: [https://www.youtube.com/watch?v=T8QT6m4hP3A](https://www.youtube.com/watch?v=T8QT6m4hP3A)
- Articles dans la revue interstices
    - **À la recherche des régions codantes**: [https://interstices.info/a-la-recherche-de-regions-codantes/](https://interstices.info/a-la-recherche-de-regions-codantes/)
    - **Stocker les données : la piste prometteuse de l'ADN**: [https://interstices.info/stocker-les-donnees-la-piste-prometteuse-de-ladn/](https://interstices.info/stocker-les-donnees-la-piste-prometteuse-de-ladn/)
- **(Courtes mais excellentes) vidéos sur les protéines**:
    - Chapitre 1 : Qu'est ce qu'une protéine ? (3:49) [https://www.youtube.com/watch?v=P_xetTpsUns](https://www.youtube.com/watch?v=P_xetTpsUns)
    - Chapitre 2 : A quoi servent les protéines ? (4:11) [https://www.youtube.com/watch?v=e_SpsJM4tlI](https://www.youtube.com/watch?v=e_SpsJM4tlI)
    - Chapitre 3 : Comment sont fabriquées les protéines ? (3:39) [https://www.youtube.com/watch?v=zfePs_hzI9g](https://www.youtube.com/watch?v=zfePs_hzI9g)
    - Chapitre 4 : Comment fonctionnent les protéines ? (3:29) [https://www.youtube.com/watch?v=sNFd7zkyrTQ](https://www.youtube.com/watch?v=sNFd7zkyrTQ)
    - Chapitre 5 : comment étudie-t-on les protéines ? (3:45) [https://www.youtube.com/watch?v=HSzRdDlebn8](https://www.youtube.com/watch?v=HSzRdDlebn8)
- **StatQuest with Josh Starmer**: [https://www.youtube.com/channel/UCtYLUTtgS3k1Fg4y5tAhLbw](https://www.youtube.com/channel/UCtYLUTtgS3k1Fg4y5tAhLbw) ("les meilleurs cours de stats de tous les temps  !" - labsquare)
- **C'est quoi le Deep learning ?** (Thibault Neveu) [https://www.youtube.com/watch?v=og5m7f1seno&list=PLpEPgC7cUJ4b1ARx8PyIQa_sdZRL2GXw5](https://www.youtube.com/watch?v=og5m7f1seno&list=PLpEPgC7cUJ4b1ARx8PyIQa_sdZRL2GXw5) ("Il est pas fluide mais il explique vraiment très très bien" - labsquare)
- **Le petit Nicolas en thèse** [https://lipn.univ-paris13.fr/~banderier/PetitNicolas/nicolas.html](https://lipn.univ-paris13.fr/~banderier/PetitNicolas/nicolas.html) pas vraiment de la bioinfo, mais pour pouvoir le retrouver facilement...


